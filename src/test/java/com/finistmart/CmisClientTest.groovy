package com.finistmart

import com.finistmart.CmisClient
import com.opencsv.CSVWriter
import org.apache.chemistry.opencmis.client.api.CmisObject
import org.apache.chemistry.opencmis.client.api.Document
import org.apache.chemistry.opencmis.client.api.Folder
import org.apache.chemistry.opencmis.commons.enums.VersioningState
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl
import org.codehaus.groovy.runtime.FlushingStreamWriter
import spock.lang.Specification
import spock.lang.Subject

class CmisClientTest extends Specification {

    @Subject
    CmisClient client = new CmisClient(

    )

    def "test createDocument"() {
        given:
        OutputStream os = new ByteArrayOutputStream();
        def writer = new CSVWriter(new FlushingStreamWriter(os))
        def data = ["First", "Second", "Third"] as String[]
        writer.writeNext(data)
        writer.writeNext(data)
        writer.writeNext(data)
        writer.close()
        def fileProps = new HashMap<String, String>() {{
            put("cmis:name", "orders.csv")
            put("cmis:objectTypeId", "cmis:document")
        }}
        ContentStreamImpl contentStream = new ContentStreamImpl();
        contentStream.setFileName("orders.csv");
        contentStream.setStream(new ByteArrayInputStream(os.toByteArray()));
        contentStream.setLength(new BigInteger("5"));
        contentStream.setMimeType("text/csv");
        def session = client.getSession("cmis", "syncro", "123456")
        def folder = (Folder) session.getObjectByPath("/User Homes/syncro/Test")
        when:
        def document = folder.createDocument(fileProps, contentStream, VersioningState.MAJOR)
        then:
        document.getId() != null
        document.delete()
    }
    def "test getContentStream"() {
        given:
        OutputStream os = new ByteArrayOutputStream();
        def writer = new CSVWriter(new FlushingStreamWriter(os))
        def data = ["First", "Second", "Third"] as String[]
        writer.writeNext(data)
        writer.writeNext(data)
        writer.writeNext(data)
        writer.close()
        def fileProps = new HashMap<String, String>() {{
            put("cmis:name", "orders.csv")
            put("cmis:objectTypeId", "cmis:document")
        }}
        ContentStreamImpl contentStream = new ContentStreamImpl();
        contentStream.setFileName("orders.csv");
        contentStream.setStream(new ByteArrayInputStream(os.toByteArray()));
        contentStream.setLength(new BigInteger("5"));
        contentStream.setMimeType("text/csv");
        def session = client.getSession("cmis", "syncro", "123456")
        def folder = (Folder) session.getObjectByPath("/User Homes/syncro/Test")
        def document = folder.createDocument(fileProps, contentStream, VersioningState.MAJOR)
        def contents = null;
        for (CmisObject cmisObject : folder.getChildren()) {
            def fetchedDocument = (Document) cmisObject;
            def stream = fetchedDocument.getContentStream(null);
            InputStream initialStream = stream.getStream();
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(initialStream, "UTF-8"));
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            contents = stringBuilder.toString();
            fetchedDocument.delete()
        }
        when:
        contents
        then:
        contents.equalsIgnoreCase("\"First\",\"Second\",\"Third\"\"First\",\"Second\",\"Third\"\"First\",\"Second\",\"Third\"")

    }
}
