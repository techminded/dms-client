package com.finistmart;

import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.CmisVersion;

import java.util.List;

public class CmisRepositoryInfo implements RepositoryInfo {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getVendorName() {
        return null;
    }

    @Override
    public String getProductName() {
        return null;
    }

    @Override
    public String getProductVersion() {
        return null;
    }

    @Override
    public String getRootFolderId() {
        return null;
    }

    @Override
    public RepositoryCapabilities getCapabilities() {
        return null;
    }

    @Override
    public AclCapabilities getAclCapabilities() {
        return null;
    }

    @Override
    public String getLatestChangeLogToken() {
        return null;
    }

    @Override
    public String getCmisVersionSupported() {
        return null;
    }

    @Override
    public CmisVersion getCmisVersion() {
        return null;
    }

    @Override
    public String getThinClientUri() {
        return null;
    }

    @Override
    public Boolean getChangesIncomplete() {
        return null;
    }

    @Override
    public List<BaseTypeId> getChangesOnType() {
        return null;
    }

    @Override
    public String getPrincipalIdAnonymous() {
        return null;
    }

    @Override
    public String getPrincipalIdAnyone() {
        return null;
    }

    @Override
    public List<ExtensionFeature> getExtensionFeatures() {
        return null;
    }

    @Override
    public List<CmisExtensionElement> getExtensions() {
        return null;
    }

    @Override
    public void setExtensions(List<CmisExtensionElement> extensions) {

    }
}
